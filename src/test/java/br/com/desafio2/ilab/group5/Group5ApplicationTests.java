package br.com.desafio2.ilab.group5;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.Null;

import java.util.Arrays;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import br.com.desafio2.ilab.group5.common.erros.AdmNotFoundException;
import br.com.desafio2.ilab.group5.dto.AdminBasicInfDTO;
import br.com.desafio2.ilab.group5.model.Administrator;
import br.com.desafio2.ilab.group5.repository.AdmRepository;
import br.com.desafio2.ilab.group5.service.AdministratorService;

@SpringBootTest
class Group5ApplicationTests {

    @Autowired
    private AdministratorService admService;

    @MockBean
    private AdmRepository admRepository;

    @Test
    @DisplayName("Retornar a lista de todos administradores usando AdministratorBasicInfDTO paginacao")
    public void listUserTeste() {
        int page = 0;
        int limit = 5;

        PageRequest paginacao = PageRequest.of(page, limit);

        List<Administrator> listAdministrators = Arrays.asList(new Administrator());
        Page<Administrator> pageAdms = new PageImpl<>(listAdministrators, paginacao, listAdministrators.size());

        when(admRepository.findAll(paginacao)).thenReturn(pageAdms);

        Assertions.assertThat(admService.list(String.valueOf(limit), page).getSize()).isEqualTo(limit);
        Assertions.assertThat(admService.list(String.valueOf(limit), page).getNumber()).isEqualTo(page);
    }

    // @Test
    // @DisplayName("Deve retornar um administradorDto pelo id")
    // public void getTeste() {

    //     Administrator newAdministrator = new Administrator();
    //     newAdministrator.setName("gabrielli");
    //     newAdministrator.setEmail("gabrielli@email.com");
    //     newAdministrator.setPassword("password");
    //     admRepository.save(newAdministrator);

    //     when(admRepository.findById(any())).thenReturn(Optional.of(newAdministrator));

    //     AdminBasicInfDTO adm = admService.findOne(newAdministrator.getId());

    //     Assertions.assertThat(adm).isInstanceOf(AdminBasicInfDTO.class);
    //     Assertions.assertThat(adm.getId()).isEqualTo(adm.getId());
    //     Assertions.assertThat(adm.getName()).isEqualTo("gabrielli");
    //     Assertions.assertThat(adm.getEmail()).isEqualTo("gabrielli@email.com");

    // }

    // @Test
    // @DisplayName("Deve salvar administrador no banco")
    // void save() {

    //     Administrator newAdministrator = new Administrator();
    //     newAdministrator.setId(1);
    //     newAdministrator.setName("gabrielli");
    //     newAdministrator.setEmail("gabrielli@email.com");
    //     newAdministrator.setPassword("password");
    //     admRepository.save(newAdministrator);

    //     when(admRepository.findById(any())).thenReturn(Optional.of(newAdministrator));

    //     Administrator adm = admRepository.findById(1).get();

    //     Assertions.assertThat(adm.getId()).isEqualTo(1);
    //     Assertions.assertThat(adm.getName()).isEqualTo("gabrielli");
    //     Assertions.assertThat(adm.getEmail()).isEqualTo("gabrielli@email.com");

    // }

    // @Test
    // @DisplayName("Deve atualizar administrador pelo id")
    // void update() {

    //     Administrator newAdministrator = new Administrator();
    //     newAdministrator.setId(1);
    //     newAdministrator.setName("gabrielli");
    //     newAdministrator.setEmail("gabrielli@email.com");
    //     newAdministrator.setPassword("password");
    //     admRepository.save(newAdministrator);

    //     when(admRepository.findById(any())).thenReturn(Optional.of(newAdministrator));
    //     Administrator adm = admRepository.findById(1).orElse(null);        
    //     adm.setName("borges");        
    //     admService.createAdmin(adm);

    //     when(admRepository.findById(any())).thenReturn(Optional.of(adm));
    //     Administrator updatedUser = admRepository.findById(1).get();

    //     Assertions.assertThat(updatedUser.getId()).isEqualTo(1);
    //     Assertions.assertThat(updatedUser.getName()).isEqualTo("borges");
    //     Assertions.assertThat(updatedUser.getEmail()).isEqualTo("gabrielli@email.com");

    // }

    @Test
    @DisplayName("Deve lançar uma exception com a mensagem 'Administrador não encontrado'")
    public void userNotFoundExceptionTest() {
        Exception exception = assertThrows(AdmNotFoundException.class, () -> {
            admService.findOne(4);
        });

        String expectedMessage = "Administrador não encontrado.";
        String actualMessage = exception.getMessage();

        Assertions.assertThat(actualMessage).isEqualTo(expectedMessage);

    }



}
