package br.com.desafio2.ilab.group5.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

public class Filters extends OncePerRequestFilter {
	private String apiKey = System.getenv("LAMBDA_API_KEY");

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		try {
			String authorization = request.getHeader("Authorization");
			if (authorization != null ) {
				
				if (authorization.contains("Bearer")) {
					Authentication auth = TokenUtils.validate(request, response);
					SecurityContextHolder.getContext().setAuthentication(auth);
				}
				
				if (authorization.equals(apiKey)) {
					UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(authorization, null, null);
					SecurityContextHolder.getContext().setAuthentication(authenticationToken);
				} 
			}
			filterChain.doFilter(request, response);

		} 	catch (Exception e) {
			// when this exception occurs, it will generate 401 status. Due to some trouble with the token. 
			System.out.println("Exception on Filters(Security): " + e.getMessage());
			
		}
	}

}