package br.com.desafio2.ilab.group5.security;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Encrypt {

	private static final String strMyKey = System.getenv("CRYPTO_API_KEY");

	public static String encrypt(String original) throws Exception {	
		Key createKey = new SecretKeySpec(strMyKey.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES");

		cipher.init(Cipher.ENCRYPT_MODE, createKey);

		cipher.update(original.getBytes());

		String originalCripto = new String(cipher.doFinal(), "UTF-8");

		byte[] passwordEncrypt = originalCripto.getBytes();

		StringBuilder cryptoHex = new StringBuilder();

		for (byte b : passwordEncrypt) {
			cryptoHex.append(Integer.toHexString(b));
		}

		return cryptoHex.toString();
	}
}
