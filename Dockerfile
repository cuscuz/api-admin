FROM openjdk:17.0.2

ARG NEW_RELIC_FILE=newrelic/newrelic.yml
ARG NEW_RELIC_JAR=newrelic/newrelic.jar
ARG JAR_FILE=target/*.jar

COPY ${NEW_RELIC_FILE} newrelic.yml
COPY ${NEW_RELIC_JAR} newrelic.jar
COPY ${JAR_FILE} app.jar

EXPOSE 8085

ENTRYPOINT [ "java","-javaagent:/newrelic.jar","-jar","/app.jar"]
